<?php
const EMD_COURSE = 8;

require_once(__DIR__.'/../../config.php');
require_once($CFG->libdir.'/externallib.php');
require_once($CFG->dirroot.'/course/lib.php');

class local_emd_webservice_external extends external_api {


    public static function get_visits() {

        $metadata = local_emd_webservice_external::get_course_metadata(EMD_COURSE);

//        var_dump(json_decode($metadata["visits"], true));
        return json_decode($metadata["visits"],true);

    }

    public static function get_visits_returns() {
//        return new external_value(PARAM_RAW, 'No Of Visitors');

        return new external_multiple_structure(
            new external_single_structure(
                array(
                    'month' => new external_value(PARAM_INT, 'Month'),
                    'visits' => new external_value(PARAM_INT, 'No Of Visitors')
                )
            )
        );
    }

    public static function get_visits_parameters() {
        return new external_function_parameters(
            array(

            )
        );
    }

    private static function get_course_metadata($courseid) {
        $handler = \core_customfield\handler::get_handler('core_course', 'course');
        // This is equivalent to the line above.
        //$handler = \core_course\customfield\course_handler::create();
        $datas = $handler->get_instance_data($courseid);
        $metadata = [];
        foreach ($datas as $data) {
            if (empty($data->get_value())) {
                continue;
            }
            $cat = $data->get_field()->get_category()->get('name');
            $metadata[$data->get_field()->get('shortname')] = $data->get_value();
        }
        return $metadata;
    }


}